var db = require('../../db')
var mail = require('../../transports').mail
var name = require('../../utils/name')
var jobs = require('../../jobs')

jobs.define('welcome-email', welcomeEmailJob)
jobs.define('signup', welcomeEmailJob)
jobs.define('resend-validation', welcomeEmailJob)

function welcomeEmailJob (job, done) {
  var data = job.attrs.data

  db().users.findOne({ email: data.to }, function (err, user) {
    if (err) {
      return job.done(err)
    }

    if (!user) {
      return job.done(new Error(`user not found for email "${data.email}"`))
    }

    var params = {
      template: 'welcome-email',
      to: {
        email: user.email,
        name: name.format(user)
      },
      lang: user.locale,
      vars: [
        { name: 'USER_NAME', content: user.firstName },
        { name: 'VALIDATE_MAIL_URL', content: data.validateUrl }
      ]
    }

    mail(params, done)
  })
}
